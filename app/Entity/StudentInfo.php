<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    //

    protected $table = 'studentsInfo';

    public function student()
    {
        return $this->belongsTo('App\Entity\User', 'studentsInfoId', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\Entity\Group', 'groupId', 'id');
    }
}
