<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $fillable = [
    	'name', 'acive'
    ];

    public function studentsInfo ()
    {
        return $this->hasMeny('App\Entity\StudentInfo', 'studentsInfoId', 'id');
    }
}
