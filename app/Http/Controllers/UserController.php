<?php

namespace App\Http\Controllers;

use App\Entity\Group;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\StudentInfo;
use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index' , ['users' => User::paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create', ['roles' => Role::all(), 'groups' => Group::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUser $request)
    {

        $user = new User();
        $role = Role::findOrFail($request->get('role'));

        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->middleName = $request->get('middle-name');
        $user->email = $request->get('middle-name');
        $user->phoneNumber = $request->get('phone-number');
        $user->password = Hash::make($request->get('password'));
        $user->dob = $request->get('dob');
        $user->isActive = $request->get('active') ? 1 : 0;

        $user->save();

        $user->attachRole($role);


        if ($role->name === ROLE::ROLE_STUDENT) {
            $group = Group::findOrFail($request->get('group'));

            $studentInfo = new StudentInfo();

            $studentInfo->group()->associate($group->id);
            $studentInfo->course = $request->get('course');
            $studentInfo->studentNumber = $request->get('studentNumber');

            $user->studentInfo()->save($studentInfo);
        }

        return redirect()->route('users-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.edit', ['user' => User::findOrFail($id), 'roles' => Role::all(), 'groups' => Group::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, $id)
    {
        $user = User::findOrFail($id);
        $role = Role::findOrFail($request->get('role'));

        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->middleName = $request->get('middle-name');
        $user->email = $request->get('middle-name');
        $user->phoneNumber = $request->get('phone-number');

        if ($request->get('password')) {
            $user->password = Hash::make($request->get('password'));
        }

        $user->dob = $request->get('dob');
        $user->isActive = $request->get('active') ? 1 : 0;

        $user->save();

        if ($role->name === ROLE::ROLE_STUDENT) {

        }

        return redirect()->route('users-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
