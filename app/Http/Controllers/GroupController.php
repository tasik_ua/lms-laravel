<?php

namespace App\Http\Controllers;

use App\Entity\Group;
use App\Http\Requests\CreateGroup;
use App\Http\Requests\UpdateGroup;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('group.index', ['groups' => Group::paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroup $request)
    {
        //
        //dd($request->all());

        $group = new Group();

        $group->name = $request->get('name');
        $group->isActive = $request->get('active') ? 1 : 0;

        $group->save();

        return redirect()->route('group-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('group.edit', [ 'group' => Group::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGroup $request, $id)
    {
        $group = Group::findOrFail($id);

        $group->name = $request->get('name');
        $group->isActive = $request->get('active') ? 1 : 0;

        $group->save();

        return redirect()->route('group-list');
    }
}
