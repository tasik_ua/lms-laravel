<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'middle-name' => 'required|string|min:3|max:100',
            'surname' => 'required|string|min:3|max:100',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:4|max:50',
            'phone-number' => 'required|min:7|max:20',
            'dob' => 'required|date',
            'role' => 'required|numeric',
            'group' => 'nullable|numeric',
            'studentNumber' => 'nullable|numeric',
            'course' => 'nullable|numeric',
        ];
    }
}
