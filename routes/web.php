<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');


Route::group(['middleware' => 'auth'], function () {

});

Route::get('/', 'HomeController@index')->name('home');


//groups
Route::get('/group-index', 'GroupController@index')->name('group-list');
Route::get('/group-create', 'GroupController@create')->name('group-create');
Route::get('/group-show', 'GroupController@show')->name('group-show');
Route::post('/group-store', 'GroupController@store')->name('group-store');
Route::get('/group-edit/{id}', 'GroupController@edit')->name('group-edit');
Route::post('/group-update/{id}', 'GroupController@update')->name('group-update');

//users
Route::get('/users-list', 'UserController@index')->name('users-list');
Route::get('/users-show/{id}', 'UserController@show' )->name('users-show');
Route::get('/users-create', 'UserController@create')->name('users-create');
Route::post('/users-store', 'UserController@store')->name('users-store');
Route::get('/users-edit/{id}', 'UserController@edit')->name('users-edit');
Route::post('/users-update/{id}', 'UserController@update')->name('users-update');

//courses


//tests


//statistics
