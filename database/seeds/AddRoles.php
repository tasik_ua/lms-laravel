<?php

use Illuminate\Database\Seeder;
use App\Entity\Role;

class AddRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new Role();
        $owner->name         = Role::ROLE_ADMINISTRATOR;
        $owner->display_name = 'Administrator'; // optional
        $owner->description  = 'Administrator of system'; // optional
        $owner->save();

        $owner = new Role();
        $owner->name         = Role::ROLE_TEACHER;
        $owner->display_name = 'Teacher'; // optional
        $owner->description  = 'Teacher of system'; // optional
        $owner->save();

        $owner = new Role();
        $owner->name         = Role::ROLE_STUDENT;
        $owner->display_name = 'Student'; // optional
        $owner->description  = 'Student of system'; // optional
        $owner->save();
    }
}
