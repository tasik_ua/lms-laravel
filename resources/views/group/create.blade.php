@extends('layouts.base')

@section('content')
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add group</h3>
                    </div>
                    <div class="card-body">
                      <form action="{{ URL::route('group-store') }}" method="post">
                      	{{ csrf_field() }}
                        <div class="form-group">
                          <label class="form-control-label">Group name</label>
                          <input type="text" name="name" placeholder="Group name" class="form-control">
                        </div>
                        <div class="i-checks">
                              <input id="active" type="checkbox" value="1" name="active" class="checkbox-template">
                              <label for="active">Active</label>
                        </div>
                       
                        <div class="form-group">       
                          <input type="submit" value="Save group" class="btn btn-primary">
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
			</div>
		</div>	


@endsection

@section('javascript')
	<script type="javascript/text">
		console.log('test');
		/*$().ready(function() {
			console.log('test');
			$('active').on('click', function (e) {
				console.log(e);
				console.log('test');
			})
		});*/
	</script>
@endsection