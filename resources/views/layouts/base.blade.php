<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PolNTU - LMS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    @yield('tylesheets')
    <!-- Bootstrap CSS-->

    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ URL::asset('css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-date-picker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <script src="{{ URL::asset('js/99347ac47f.js') }}"></script>
    <!-- Font Icons CSS-->
    <link rel="stylesheet" href="{{ URL::asset('css/icons.css') }}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>![endif]-->
</head>
<body>
<div class="page home-page" style="height:100%">
    <!-- Main Navbar-->
    <header class="header">
        <nav class="navbar">
            <!-- Search Box-->
            <div class="search-box">
                <button class="dismiss"><i class="icon-close"></i></button>
                <form id="searchForm" action="#" role="search">
                    <input type="search" placeholder="What are you looking for..." class="form-control">
                </form>
            </div>
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <!-- Navbar Header-->
                    <div class="navbar-header">
                        <!-- Navbar Brand --><a href="#" class="navbar-brand">
                            <div class="brand-text brand-big hidden-lg-down"><strong>PolNTU </strong> Lerning Managemnt System</div>
                            <div class="brand-text brand-small"><strong>PolNTU</strong></div></a>
                        <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                    </div>
                    <!-- Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <!-- Search-->
                        <!--<li class="nav-item d-flex align-items-center"><a id="search" href="#"><i class="icon-search"></i></a></li>
                        <!-- Notifications-->

                        <!-- Messages                        -->

                        <!-- Logout    -->
                        <li class="nav-item"><a href="#" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="page-content d-flex align-items-stretch" style="min-height: 89.5%">
        <!-- Side Navbar -->
        <nav class="side-navbar" style="min-height: 100%">
            <!-- Sidebar Header-->
            <div class="sidebar-header d-flex align-items-center">
                <div class="avatar"><img src="/img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
                <div class="title">
                    <h1 class="h4">Mark Stephen</h1>
                </div>
            </div>
            <!-- Sidebar Navidation Menus-->
            <ul class="list-unstyled">
                <li><a href="#users" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-users"></i>Users </a>
                    <ul id="users" class="collapse list-unstyled">
                        <li><a href="{{ URL::route('users-list') }}">User list</a></li>
                        <li><a href="{{ URL::route('users-create') }}">Add user</a></li>
                    </ul>
                </li>

                <li><a href="#courses" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-book"></i>Courses </a>
                    <ul id="courses" class="collapse list-unstyled">
                        <li><a href="#">Courses list</a></li>
                        <li><a href="#">Add new course</a></li>
                    </ul>
                </li>
                <li><a href="#groups" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-graduation-cap"></i>Groups </a>
                    <ul id="groups" class="collapse list-unstyled">
                        <li><a href="{{ URL::route('group-list') }}">Group list</a></li>
                        <li><a href="{{ URL::route('group-create')}}">Add new group</a></li>
                    </ul>
                </li>
                <li><a href="#test" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-check-square-o"></i>Tests</a>
                    <ul id="test" class="collapse list-unstyled">
                        <li><a href="#">Test list</a></li>
                        <li><a href="#">Add new test</a></li>
                    </ul>
                </li>
                <li><a href="#stats" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-pie-chart"></i>Statistics</a>
                    <ul id="stats" class="collapse list-unstyled">
                        <li><a href="#">Test</a></li>
                        <li><a href="#">Users</a></li>
                        <li><a href="#">Courses</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="content-inner">
            <!-- Page Header-->
            <header class="page-header">
                <div class="container-fluid">
                    <h2 class="no-margin-bottom">Dashboard</h2>
                </div>
            </header>
            <!-- Dashboard Counts Section-->
            <section class="dashboard-counts no-padding-bottom" style="min-height: 78%">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </section>


            <!-- Page Footer-->
            <footer class="main-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>PolNTU &copy; 2017</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<!-- Javascript files-->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/tether.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-date-picker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.cookie.j') }}s"> </script>
<script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
<!--<script src="{{ URL::asset('js/Chart.min.js') }}"></script>-->
<!--<script src="{{ URL::asset('js/charts-home.js') }}"></script>-->
<script src="{{ URL::asset('js/front.js') }}"></script>
<script src="{{ URL::asset('js/custom.js') }}"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
<!---->

@yield('javascript');

<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');


    jQuery(document).ready(function() {
        $('.js-datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    });

</script>
</body>
</html>