@extends('layouts.base')

@section('content')

    <div class="col-lg-12">
        <div class="card">
            <div class="card-close">
                <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                </div>
            </div>
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Users</h3>
            </div>
            <div class="card-body">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Full Name</th>
                        <th>Active</th>
                        <th>Role</th>
                        <th>Creted at</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name . ' ' . $user->middleName . ' ' . $user->surname}}</td>
                                <td>{!!  $user->isActive ? '<span class="label label-primary">Yes</span>' : '<span class="label label-default">No</span>' !!}</td>
                                <td>{{ $user->roles[0]->display_name }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                    <a href="{{ URL::route('users-edit', ['id' => $user->id]) }}" class="btn btn-info btn-sm">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>


                {{ $users->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection