@extends('layouts.base')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <!-- Basic Form-->
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Update user</h3>
          </div>
          <div class="card-body">
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif

            <form action="{{ URL::route('users-update', $user->id) }}" method="post">
              {{ csrf_field() }}

              <div class="form-group">
                <label class="form-control-label">Name</label>
                <input type="text" name="name" placeholder="Name" value="{{ $user->name }}" class="form-control">
              </div>

              <div class="form-group">
                <label class="form-control-label">Middle name</label>
                <input type="text" name="middle-name" placeholder="Middle name"  value="{{ $user->middleName }}" class="form-control">
              </div>

              <div class="form-group">
                <label class="form-control-label">Surname</label>
                <input type="text" name="surname" placeholder="Surname" value="{{ $user->surname }}" class="form-control">
              </div>

              <div class="form-group">
                <label class="form-control-label">Email</label>
                <input type="text" name="email" placeholder="email"  value="{{ $user->email }}" class="form-control">
              </div>

              <div class="form-group">
                <label class="form-control-label">Phone number</label>
                <input type="text" name="phone-number" placeholder="Phone number" value="{{ $user->phoneNumber }}" class="form-control">
              </div>

              <div class="form-group">
                <label class="form-control-label">Date of birth</label>
                <input type="text" name="dob" placeholder="Date of birth"  value="{{ $user->dob }}" class="form-control js-datepicker"  >
              </div>

              <div class="form-group">
                <label class="form-control-label">Password</label>
                <input type="text" name="password" placeholder="Password" class="form-control">
              </div>

              <div class="i-checks">
                <input id="active" type="checkbox" value="1" {{ $user->isActive ? 'checked="checked"': '' }} name="active" class="checkbox-template">
                <label for="active">Active</label>
              </div>

              <div class="form-group">
                <label class="form-control-label">Role</label>
                <select name="role" id="role"  class="form-control">
                  @foreach($roles as $role)
                    @if($user->roles[0]->id === $role->id)
                      <option value="{{ $role->id }}" selected> {{$role->display_name}} </option>
                    @else
                      <option value="{{ $role->id }}"> {{$role->display_name}} </option>
                    @endif
                  @endforeach
                </select>
              </div>

              <div id="studentInfoBlock" class="hidden-block" style="display: none">
                <div class="form-group">
                  <label class="form-control-label">Group</label>
                  <select name="group" id="group"  class="form-control">
                    @foreach($groups as $group)
                      <option value="{{ $group->id }}"> {{$group->name}} </option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label class="form-control-label">Course</label>
                  <select name="course" id="course"  class="form-control">
                    @for($i=1; $i<7; $i++)
                      <option value="{{ $i }}"> {{ $i }} course</option>
                    @endfor
                  </select>
                </div>

                <div class="form-group">
                  <label class="form-control-label">Student number</label>
                  <input type="text" name="studentNumber" placeholder="Student number" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <input type="submit" value="Update user" class="btn btn-primary">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('javascript')
  <script type="javascript/text">
      console.log('test);
  </script>
@endsection